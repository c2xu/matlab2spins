%MATLAB2SPINS2D   Matlab to SPINS grid ordering, for a 2D domain with
% periodic/free-slip conditions in the x-direction and free-slip/no-slip 
% conditions in the z-direction and a flat bottom boundary.
%

%% Writing data to disk

% Remember to specify case name first
if strcmp(casename,'')
    error('Please specify case name.')
else
    mkdir(['../' casename]), cd(['../' casename])
end

dspins = permute(rho2,[2,1]);
uspins = permute(u2,[2,1]);
wspins = permute(w2,[2,1]);

fid = fopen('rho.orig','wb'); fwrite(fid,dspins,'double'); fclose(fid);
fid = fopen('u.orig','wb'); fwrite(fid,uspins,'double'); fclose(fid);
fid = fopen('w.orig','wb'); fwrite(fid,wspins,'double'); fclose(fid);

if f~=0
    vspins = permute(v2,[2,1]);
    fid = fopen('v.orig','wb'); fwrite(fid,vspins,'double'); fclose(fid);
end

%% Writing wave parameters to wave.conf

fwave = fopen('wave.conf','wt');
switch wave
    case 'lin'
        if standing
            fprintf(fwave,'type of wave = standing \n');
        else
            fprintf(fwave,'type of wave = traveling \n');
        end
        if f~=0, fprintf(fwave,'rotation on \n'); end
        
%         fprintf(fwave,'mode number = %12.8f \n', md);
%         fprintf(fwave,'wavelength = %12.8f \n', wl);
        fprintf(fwave,'amplitude = %12.4f \n', amp);
        if right
            fprintf(fwave,'direction = right \n');
        else
            fprintf(fwave,'direction = left \n');
        end
        fprintf(fwave,'\n%% Envelop function %% \n');
        fprintf(fwave,'%s \n\n', func2str(env));
        
        fprintf(fwave,['%-12s','%-12s',...
            repmat(['%-',num2str(12*length(md)),'s'],1,2),'\n'],...
            'lambda (m)','k (1/m)','c_p (cm/s)','c_g (cm/s)');
        fprintf(fwave,['%-12s','%-12s',...
            repmat('mode-%g      ',1,length(md)),...
            repmat('mode-%g      ',1,length(md)),'\n'],...
            '','',md,md);
        for cntr = 1:length(wl)
            fprintf(fwave,[repmat('%-12.2f',1,2+2*length(md)),'\n'],...
                wl(cntr),2*pi/wl(cntr),cp(cntr,:)*100,cg(cntr,:)*100);
        end
        
    case 'wnl'
        % to be added in the future...
        
    case 'djl'
        fprintf(fwave,'APE = %12.8f \n', A);
        fprintf(fwave,'eta_max = %.4f \n', max(abs(eta(:))));
        fprintf(fwave,'c = %.4f \n', c);
        fprintf(fwave,'u_max = %.4f \n', max(u(:)));
        fprintf(fwave,'u_min = %.4f \n', min(u(:)));
        fprintf(fwave,'Ri_min = %.4f \n', min(ri(:)));
        fprintf(fwave,'L_wave = %.4f \n', wavelength);
        fprintf(fwave,'L_Ri = %.4f \n', L_Ri);
        fprintf(fwave,'L_Ri/L_wave = %.4f \n', L_ratio);
end

fprintf(fwave,'L = %12.8f \n', L);
fprintf(fwave,'H = %12.8f \n', H);
fprintf(fwave,'Nx_wave = %d \n', NX);
fprintf(fwave,'Nz_wave = %d \n', NZ);

fprintf(fwave,'\n%% Density profile %% \n');
fprintf(fwave,'%s \n', func2str(md_density));
% fprintf(fwave,'\n%%% Primary pycnocline %%% \n');
fprintf(fwave,'drho = %12.8f \n', drho);
fprintf(fwave,'z0 = %12.8f \n', z0);
fprintf(fwave,'d = %12.8f \n', d);
% fprintf(fwave,'\n%%% Secondary pycnocline %%% \n');
% fprintf(fwave,'drho2 = %12.8f \n', drho2);
% fprintf(fwave,'z02 = %12.8f \n', z02);
% fprintf(fwave,'d2 = %12.8f \n', d2);

fprintf(fwave,'\n%% Background current %% \n');
fprintf(fwave,'%s \n\n', func2str(md_u));

fprintf(fwave,'interp_method = %s \n\n', method);
fclose(fwave);

%% Writing SPINS parameters to spins.conf

fid = fopen('spins.conf','wt');
fprintf(fid,'Lx = %12.8f \n',Lx);
if f~=0, fprintf(fid,'Ly = 1 \n'); end
fprintf(fid,'Lz = %12.8f \n',Lz);
fprintf(fid,'Nx = %d \n',Nx);
if f~=0, fprintf(fid,'Ny = 1 \n'); end
fprintf(fid,'Nz = %d \n',Nz);

if strcmp(wave,'lin') && standing
    fprintf(fid,'type_x = FREE_SLIP \n');
else
    fprintf(fid,'type_x = FOURIER \n');
end
if f~=0
    fprintf(fid,'type_y = FOURIER \n');
end
if slip
    fprintf(fid,'type_z = FREE_SLIP \n');
else
    fprintf(fid,'type_z = NO_SLIP \n');
end

fprintf(fid,'min_x = %12.8f\n',0);
if f~=0, fprintf(fid,'min_y = %12.8f\n',0); end
fprintf(fid,'min_z = %12.8f\n',0);
fprintf(fid,'mapped_grid = false\n');

fprintf(fid,'file_type = MATLAB\n');
fprintf(fid,'u_file = u.orig\n');
if f~=0, fprintf(fid,'v_file = v.orig\n'); end
fprintf(fid,'w_file = w.orig\n');
fprintf(fid,'rho_file = rho.orig\n');

fprintf(fid,'g = %12.8f \n',g);
fprintf(fid,'rot_f = %12.8f \n',f);
fprintf(fid,'rho_0 = %12.8f \n',1000);
fprintf(fid,'visco = %12.8f \n',nu);
fprintf(fid,'kappa_rho = %12.8f \n',kappa);
fprintf(fid,'perturb = %f \n',pert);

% fprintf(fid,'init_time = %12.8f\n',init_time);
fprintf(fid,'final_time = %12.8f\n',final_time);
fprintf(fid,'plot_interval = %12.8f \n',plot_interval);
fprintf(fid,'restart = false \n');
fclose(fid);

cd ../matlab2spins

