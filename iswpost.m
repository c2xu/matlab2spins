%% Post processing

% Create the temporary wavenumbers
ks = (pi/L) * [0:(NX-1) -NX:-1];
% the minus is to account for the ordering of the z grid
ms = -(pi/H) * [0:(NZ-1) -NZ:-1]';

KS = repmat(ks, [2*NZ 1]);
MS = repmat(ms, [1 2*NX]);

% Extend the variable of interest
array = [eta -fliplr(eta); -flipud(eta) rot90(eta,2)];
ARRAY = fft2(array);
array = ifft2(1i*KS.*ARRAY); etax = array(1:NZ, 1:NX);
array = ifft2(1i*MS.*ARRAY); etaz = array(1:NZ, 1:NX);

up  = md_u (z-eta);
%uu  = md_u (z);

% Velocities
u=real(up+(c-up).*etaz); %uwave=u-c;
w=real(up.*(etax)-c*etax);

% Velocity gradients
array = [u fliplr(u); flipud(u) rot90(u,2)];
ARRAY = fft2(array);
%array = ifft2(1i*KS.*ARRAY); ux = real(array(1:NZ, 1:NX));
array = ifft2(1i*MS.*ARRAY); uz = real(array(1:NZ, 1:NX));

% Density
den=md_density(z-eta);

% Density gradient
array = [den fliplr(den); flipud(den) rot90(den,2)];
ARRAY = fft2(array);
%array = ifft2(1i*KS.*ARRAY); denx = real(array(1:NZ, 1:NX));
array = ifft2(1i*MS.*ARRAY); denz = real(array(1:NZ, 1:NX));

% N2 and gradient Richardson number
n2p=-9.81*denz;
ri=n2p./(uz.*uz);

% Filter the Richardson number since having Ri<0.25 is irrelevant if there
% is no stratification (N^2 is too small)
ri=ri.*(n2p>.05&abs(uz)>1e-3)+100.*(n2p<.05|abs(uz)<1e-3);
