%DRIVER     Input parameters and main driver file for MATLAB2SPINS2D, with
% parameters read from a text file.
%
% Currently the code takes parameters for traveling linear waves only.
%
% Last updated: July 12, 2018
%

clearvars

test = true;    % set to false to write data to disk

par = readtable('poincare.txt');    % this may not work for R2017b or later

%% Loop independent parameters

% MATLAB grid
L = 4;          % length of solitary/linear wave domain
H = 0.5;        % depth of solitary/linear wave domain, must be positive
NX = 512;       % x-grid of DJL/TG solver
NZ = 256;       % z-grid of DJL/TG solver (Cheb grid is used in TG solver)

% SPINS grid
Lx = L;         % length of computational domain
Lz = H;         % depth of computational domain
Nx = 4096;      % x-grid for SPINS
Nz = 256;       % z-grid for SPINS

% Background current
md_u=@(z) zeros(size(z));
md_uz=@(z) zeros(size(z));
md_uzz=@(z) zeros(size(z));

% Miscellaneous parameters
wave = 'lin';
standing = false;	% true (false) for standing (traveling) waves
right = true;       % true (false) for rightward (leftward) propagation
f   = 1e-1;         % rotation
find_cg = false;    % set false to turn it off

% Interpolation related parameters
ydir = false;               % true for 3D, false for 2D (only 2D for now)
slip = true;                % true for free-slip, false for no-slip
method = 'spline';          % interpolation method
% 'no_interp': works for TG solver only if no-slip in z (i.e. slip==0),
%       works for DJL solver only if free-slip in z (i.e. slip==1).
% 'spectral': does not work for TG solver as it uses Cheb grid in z.
% 'nearest', 'linear', 'spline' and 'cubic': Matlab built-in interpolation
%       methods; 'spline' has the highest accuracy.
    
% SPINS parameters
g     = 9.81;
nu    = 1e-6;
kappa = 2e-7;
pert  = 0.00;
init_time  = 0;
final_time = 100;
plot_interval = 1;

%% Loop through different cases

for numcase = 1:height(par)
    casename = char(par.casename(numcase));
    
    % Primary pycnocline
    drho = par.drho(numcase);   % density difference
    z0 = par.z0(numcase);       % pycnocline center
    d = par.d(numcase);         % pycnocline thickness
    
    % Density profile
    md_density = @(z) 1 - 0.5*drho*tanh((z-z0)/d);
    md_d_density = @(z) - 0.5*drho *sech((z-z0 )/d ).*sech((z-z0 )/d )/d;
    
    wl   = par.wl(numcase);     % wavelength for traveling waves
    % Note: Even for traveling waves, L must still be divisible by wl,
    % unless appropriate envelop functions are specified.
    
    md  = par.md(numcase);      % vertical mode number
    amp = par.amp(numcase);     % maximum amplitude
    
    % Envelop function and its x-derivative
    env = @(x,num_mode) amp;
    envx = @(x,num_mode) 0*amp;

    % Taylor-Goldstein solver
    tg
    
    % Interpolation    
    if ~ydir, spins_interp2d, end
    
    % SPINS grid ordering
    if ~test, matlab2spins2d, end
end

if ~test, quit, end
