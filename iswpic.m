%% Output parameters

fprintf('eta_max = %.4f, c = %.4f, u_max = %.4f, u_min = %.4f, Ri_min = %.4f\n',...
    max(abs(eta(:))),c,max(u(:)),min(u(:)),min(ri(:)))

% ISW width
utop = u(1,:);              % u along the top boundary
[a,b] = max(utop);          % a: maximum velocity, b: index of max velocity
[~,idx] = sort(abs(utop-a/2));
wavelength = abs(xx(idx(1))-xx(idx(2)));
% [~,d] = min(abs(utop-a/2));	% d: index of location of utop/2
% wavelength = 2*abs(xx(b)-xx(d));
fprintf('wavelength = %.4f, ',wavelength);

% Length of unstable region
[cont_x, cont_y] = find_contour(xx,zz,ri,0.25);
L_Ri = max(cont_x(:))-min(cont_x(:));
L_ratio = L_Ri/wavelength;
fprintf('length of low Ri region = %.4f, ',L_Ri);
fprintf('ratio = %.4f\n',L_ratio);

%% Plots

if test
    set(0,'defaulttextinterpreter','latex')
    
    figure(200), clf, colormap temperature
    set(gcf,'units','inches','position',[1 4.5 14 5.5])
    subplot(1,2,1)
    pcolor(x,z,den), shading flat
    axis([0 Lx 0 Lz])
    caxis(1+[-1 1]*(max(abs(den(:)))-1)), colorbar southoutside
    xlabel('$x$ (m)'), ylabel('$z$ (m)')
    title('(a) Density field')
    set(gca,'fontname','times','fontsize',18)
    
    subplot(1,2,2)
    pcolor(x,z,u), shading flat
    caxis([-1 1]*max(abs(u(:)))), colorbar southoutside
    hold on
    contour(x,z,den,[1,1],'--k','linewidth',1.2)
    axis([0 Lx 0 Lz])
    xlabel('$x$ (m)'), ylabel('$z$ (m)')
    title('(b) Horizontal velocity field')
    set(gca,'fontname','times','fontsize',18)
    
    figure(202), clf,
    colormap(flipud(gray))
    set(gcf,'units','inches','position',[1 1 10.5 3.5])
    pcolor(x,z,ri), shading flat
    caxis([min(ri(:)) 1]), colorbar
    hold on
    contour(x,z,den,[1,1],'--r','linewidth',1.2)
    contour(x,z,ri,[.1,.25],'b','linewidth',1.2)
    axis([0 Lx 0 Lz])
    xlabel('$x$ (m)'), ylabel('$z$ (m)')
    %title('(c) Gradient Richardson number')
    set(gca,'fontname','times','fontsize',18)
end
