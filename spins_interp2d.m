%SPINS_INTERP2D      Interpolation of u, v, w and rho fields from DJL/TG 
% solver onto a regular or Cheb grid.
%
% Note: this script works for 2D only
% 
% Last updated: June 29, 2018
%

%% Preparing grid for interpolation

x1d = Lx*(0.5:Nx-0.5)/Nx;           % periodic/free-slip in x
if slip
    z1d = Lz*(0.5:Nz-0.5)/Nz;       % free-slip in z
else
    [~,ztmp] = cheb(Nz);            % no-slip in z: size(z1d)==Nz+1
    z1d = flipud(0.5*Lz*(ztmp+1));  % so that z1d is in increasing order
end
[x2d,z2d] = meshgrid(x1d,z1d);      % 2D grid in Matlab's meshgrid format

%% Interpolation

switch method
    case 'no_interp'
        % No interpolation. Check grid and boundary conditions first.
        if Nx ~= NX, error('x-grid must agree if no interpolation.'), end
        if Nz ~= NZ, error('z-grid must agree if no interpolation.'), end
        if strcmp(wave,'lin') && slip
            error('Use no-slip for linear theory if no interpolation.')
        elseif strcmp(wave,'djl') && ~slip
            error('Use free-slip for ISWs if no interpolation.')
        end
        
        u2 = flipud(u);
        w2 = flipud(w);
        rho2 = flipud(den);
        if f~=0, v2 = flipud(v); end
        
    case 'spectral'
        if strcmp(wave,'lin')
            error('Spectral interpolation does not work for TG solver.')
        end
        
        % Interpolation in x-direction
        u1   = resize_x(L,NX,NZ,'u','periodic',u,Nx);
        w1   = resize_x(L,NX,NZ,'w','periodic',w,Nx);
        rho1 = resize_x(L,NX,NZ,'rho','periodic',den,Nx);
        
        % Interpolation in z-direction
        % 1) The input size in x-direction is now Nx instead of NX
        % 2) Need to flipud since z in DJL solver is defined upside down
        u2   = flipud(resize_z(H,Nx,NZ,'u',u1,Nz));
        w2   = flipud(resize_z(H,Nx,NZ,'w',w1,Nz));
        rho2 = flipud(resize_z(H,Nx,NZ,'rho',rho1,Nz));
        
    otherwise
        % Expand the grid first
        x1 = [x(:,1)-2*dx	x(:,1)-dx   x	x(:,end)+dx   x(:,end)+2*dx];
        z1 = [z(:,1)        z(:,1)      z   z(:,end)      z(:,end)];
        x1 = [x1(1,:);      x1(1,:);    x1; x1(end,:);    x1(end,:)];
        z1 = [2*z1(1,:)-z1(3,:);        2*z1(1,:)-z1(2,:);      z1; ...
            2*z1(end,:)-z1(end-1,:);	2*z1(end,:)-z1(end-2,:)];
        
        % This part takes care of the BCs (not required for 'spectral')
        if strcmp(wave,'lin') && standing
            % Free-slip (Dirichlet for u, Neumann for v, w and den) in x
            u1   = [ -u(:,2)   -u(:,1)   u    -u(:,end)   -u(:,end-1)];
            w1   = [  w(:,2)    w(:,1)   w     w(:,end)    w(:,end-1)];
            den1 = [den(:,2)  den(:,1)  den  den(:,end)  den(:,end-1)];
            if f~=0
                v1 = [v(:,2)    v(:,1)   v     v(:,end)    v(:,end-1)];
            end
        else
            % Periodic in x-direction
            u1   = [  u(:,(end-1):end)   u     u(:,1:2)];
            w1   = [  w(:,(end-1):end)   w     w(:,1:2)];
            den1 = [den(:,(end-1):end)  den  den(:,1:2)];
            if f~=0
                v1 = [v(:,(end-1):end)   v     v(:,1:2)];
            end
        end
        
        % Free/no-slip (Dirichlet for w, Neumann for u, v and den) in z
        % SPINS will (hopefully) take care of the no-slip condition
        u1   = [  u1(2,:);   u1(1,:);  u1;    u1(end,:);   u1(end-1,:)];
        w1   = [ -w1(2,:);  -w1(1,:);  w1;   -w1(end,:);  -w1(end-1,:)];
        den1 = [den1(2,:); den1(1,:); den1; den1(end,:); den1(end-1,:)];
        if f~=0
            v1 = [v1(2,:);   v1(1,:);  v1;    v1(end,:);   v1(end-1,:)];
        end
        
        % Interpolation
        u2   = interp2(x1,z1,u1,x2d,z2d,method);
        w2   = interp2(x1,z1,w1,x2d,z2d,method);
        rho2 = interp2(x1,z1,den1,x2d,z2d,method);
        if f~=0, v2 = interp2(x1,z1,v1,x2d,z2d,method); end
end

% figure(100), clf, colormap darkjet
% subplot(3,1,1), pcolor(x2d,z2d,rho2), shading flat
% subplot(3,1,2), pcolor(x2d,z2d,u2), shading flat
% subplot(3,1,3), pcolor(x2d,z2d,w2), shading flat
