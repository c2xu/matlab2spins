casename,       Lx,  Lz,   Nx,    Nz,   z0,    d,     amp,  wl,  amp_seiche
base_crest,     10,  0.4,  4096,  512,  0.32,  0.01,  0.1,  1,   0.04
base_trough,    10,  0.4,  4096,  512,  0.32,  0.01,  0.1,  1,   -0.04
base_noseiche,  10,  0.4,  4096,  512,  0.32,  0.01,  0.1,  1,   0.00


