function field = resize_z(Lz0, Nx0, Nz0, field_name, field, Nz1)
%% RESIZE_Z  Increase or decrease the resolution of 'field' in the z dimension.
%
% Assumptions:
%  - if 2D, then must be in x-z plane
%  - Boundary condition must be free-slip

% % read parameters
% params = spins_params();
% 
% % shorten some parameters
% type_z = params.type_z;
% Lz0 = params.Lz;
% Nx0 = size(field, 1);
% Nz0 = params.Nz;
dz0 = Lz0/Nz0;
% try
%     Ny0 = params.Ny;
% catch
%     Ny0 = 1;
% end
Ny0 = 1;

% % error check
% if ~strcmp(type_z, 'FREE_SLIP') && ~strcmp(type_z, 'FOURIER')
%     error('X boundary condition is not FREE_SLIP or FOURIER.')
% end

% check sizes
mult = Nz1/Nz0;
if mult > 1
    increase_points = true;
else
    increase_points = false;
end

% permute to match SPINS output files
field = permute(field, [2 1]);
% reshape 2D to match the shape of a 3D field
if Ny0 == 1
    field = reshape(field, [Nx0 1 Nz0]);
end
% permute to put extending dimension in the first dimension
field = permute(field, [3 2 1]);

% extension types
odd_ext  = @(f) [f; -flipud(f)];
even_ext = @(f) [f;  flipud(f)];

% double the field
if strcmp(field_name, 'w')
    field = odd_ext(field);
else
    field = even_ext(field);
end

% phase shift because grid is cell-centered, and take fft
dz1 = dz0/mult;
kzs = fftfreq(2*Nz0, dz0);
phase_corr = exp(-1i*kzs*(dz0/2-dz1/2)).';
field = bsxfun(@times, fft(field), phase_corr);

% pad or truncate
if increase_points
    % pad with zeros
    field = [field(1:Nz0,:,:); zeros(2*Nz1-2*Nz0,Ny0,Nx0); field(end-Nz0+1:end,:,:)];
else
    % truncate high frequencies
    field = [field(1:Nz1,:,:); field(end-Nz1+1:end,:,:)];
end

% take ifft
field = mult*real(ifft(field));
field = field(1:Nz1,:,:);
% permute back
field = permute(field, [3 2 1]);
% return field to 2D if it was
if Ny0 == 1
    field = reshape(field,[Nx0 Nz1]);
end
field = permute(field, [2 1]);
