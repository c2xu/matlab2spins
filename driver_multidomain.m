%DRIVER     Input parameters and main driver file for MATLAB2SPINS2D with
% multiple domains combined together.
%
% Last updated: July 9, 2018
%

clearvars

test = true;    % set to true if just testing, set to false and specify
                % casename below to write data to disk

casename = '';  % specify the case name here

%% Stratification and background current

H = 0.2;        % depth of solitary/linear wave domain, must be positive

% Primary pycnocline
drho = 0.01;    % density difference
z0 = 0.18;      % pycnocline center
d = 0.005;      % pycnocline thickness

% Secondary pycnocline
drho2 = 0.0*drho;   % set to 0 if there is only one pycnocline
z02 = 0.19;
d2 = 0.0025;

% Density profile
md_density = @(z) 1 - 0.5*drho*tanh((z-z0)/d) - 0.5*drho2*tanh((z-z02)/d2);
md_d_density = @(z) - 0.5*drho *sech((z-z0 )/d ).*sech((z-z0 )/d )/d...
                    - 0.5*drho2*sech((z-z02)/d2).*sech((z-z02)/d2)/d2;
% n2 = @(z) 9.81*0.5*drho *sech((z-z0 )/d ).*sech((z-z0 )/d )/d...
%        + 9.81*0.5*drho2*sech((z-z02)/d2).*sech((z-z02)/d2)/d2;

% uamp=0.0;
md_u=@(z) zeros(size(z));
md_uz=@(z) zeros(size(z));
md_uzz=@(z) zeros(size(z));

%% Overall domain

% Grid parameters
L1 = 4;             % length of the first domain
L2 = 4;             % length of the second domain
Lx = L1 + L2;       % overall length of the computational domain
Lz = H;             % depth of computational domain (must be the same as H)
Nx = 8192;          % x-grid for SPINS
Nz = 512;           % z-grid for SPINS

% Interpolation
ydir = false;               % true for 3D, false for 2D (only 2D for now)
slip = true;                % true for free-slip, false for no-slip
f    = 0;
method = 'cubic';          % interpolation method
% Note: 'spline' does not work for combined domains!!!
% 'no_interp': works for TG solver only if no-slip in z (i.e. slip==0),
%       works for DJL solver only if free-slip in z (i.e. slip==1).
% 'spectral': does not work for TG solver as it uses Cheb grid in z.
% 'nearest', 'linear', 'spline' and 'cubic': Matlab built-in interpolation
%       methods; 'spline' has the highest accuracy.

%% First domain

% MATLAB grid
L  = L1;            % length of solitary/linear wave domain
NX = 512;           % x-grid of DJL/TG solver
NZ = 256;           % z-grid of DJL/TG solver (Cheb grid is used in TG solver)
wave = 'djl';       % type of wave, needed for interpolation

A = 2e-4;           % APE
if test
    verbose = 1;    % show progress during solving
else
    verbose = 0;
end

% In the DJL solver, the z-grid is equally spaced and is defined
% from H to 0 in decreasing order
get_eta
iswpost
iswpic

if ~ydir, spins_interp2d, end
u2(isnan(u2)) = 0;                                  u3 = u2;
w2(isnan(w2)) = 0;                                  w3 = w2;
rho2(isnan(rho2)) = md_density(z2d(isnan(rho2)));	rho3 = rho2;

clear L NX NZ wave u2 w2 rho2

%% Second domain

% MATLAB grid
L  = Lx;            % length of solitary/linear wave domain
NX = 4096;          % x-grid of DJL/TG solver
NZ = 256;           % z-grid of DJL/TG solver (Cheb grid is used in TG solver)
wave = 'lin';       % type of wave, needed for interpolation

standing = false;	% true (false) for standing (traveling) waves
% 'true': free-slip side walls, specify 'md_x' below
% 'false': periodic side walls, specify 'wl' and below

md_x = 2;           % horizontal mode number for standing waves
wl   = .07;         % wavelength for traveling waves (can be a vector)
% Note: Even for traveling waves, L must still be divisible by wl,
% unless appropriate envelop functions are specified.

md  = 1;            % vertical mode number (can be a vector)
amp = .001;         % maximum amplitude
right = true;       % true (false) for rightward (leftward) propagation

% Envelop function and its x-derivative
L_left  = L1 + 1;                   % left boundary of the envelope
L_right = Lx - 1;                   % right boundary of the envelope
dd = .2;                            % width of the envelope boundary
env = @(x,num_mode) amp ...
            *(.5*tanh((x-L_left)/dd)/num_mode ...
            - .5*tanh((x-L_right)/dd)/num_mode);
envx = @(x,num_mode) amp ...
            *(.5*(1 - tanh((x-L_left)/dd).^2)/dd/num_mode ...
            - .5*(1 - tanh((x-L_right)/dd).^2)/dd/num_mode);

find_cg = false;        % set false to turn it off, since computing c_g
                        % will slow things down significantly

% Taylor-Goldstein solver, the z-grid is Chebyshev and is defined
% from H to 0 in decreasing order
tg

if ~ydir, spins_interp2d, end
u3 = u2 + u3;                           u2 = u3;
w3 = w2 + w3;                           w2 = w3;
rho3 = rho2 + rho3 - md_density(z2d);   rho2 = rho3;

if test
    figure(100), clf, colormap darkjet
    subplot(3,1,1), pcolor(x2d,z2d,rho3), shading flat
    subplot(3,1,2), pcolor(x2d,z2d,u3), shading flat
    subplot(3,1,3), pcolor(x2d,z2d,w3), shading flat
end

%% SPINS grid ordering

g     = 9.81;
nu    = 1e-6;
kappa = 2e-7;
pert  = 0.00;
init_time  = 0;
final_time = 100;
plot_interval = 1;

if ~test, matlab2spins2d, quit, end
