%TG     Taylor-Goldstein equation solver.
% 
% Last updated: June 29, 2018
%

if standing
    wl = L./md_x;               % Compute wavelengths for standing waves
elseif mod(L,wl) ~= 0
    warning(['L is not divisible by wl. ' ...
        'Make sure appropriate envelop functions are specified.'])
end

%% Prepare loop-independent variables and matrices

% Chebyshev matrix and grid
[D,zz] = cheb(NZ);
Dphys = D/(0.5*H);          D2phys=Dphys^2;
xphys = L*(0.5:NX-0.5)/NX;  dx = xphys(2)-xphys(1);
zphys = 0.5*H*(zz+1);
[x,z]=meshgrid(xphys,zphys);
% To be consistent with the DJL solver, here z is also defined from H to 0
% in decreasing order.

% Background profiles
% rho  = md_density(zphys);
rhoz = md_d_density(zphys);

Uall = md_u(zphys);
% Uz   = md_uz(zphys);
Uzz  = md_uzz(zphys);

N2 = -9.81*rhoz;
% Ri = N2./Uz.^2;

% Dirichlet conditions
D2phys = D2phys(2:NZ,2:NZ);
U = Uall(2:NZ);	Uzz = Uzz(2:NZ);
N2 = N2(2:NZ);  N2(N2<0) = zeros(sum(N2<0),1);

% Matrices that do not involve the wave number k
if any(U(:)) ~= 0
    warning('Non-zero background current has not been tested yet.')
    if f ~= 0
        f = 0; warning('Rotation will be ignored if U~=0.')
    end
    A11 = sparse(diag(N2));         B11 = sparse(zeros(NZ-1,NZ-1));
    A12 = sparse(diag(U));          B12 = sparse(eye(NZ-1));
    A22 = sparse(-eye(NZ-1));       B22 = sparse(zeros(NZ-1,NZ-1));
elseif f == 0
    A = sparse(-diag(N2));
end

% Pre-allocate memory space
cp = zeros(length(wl),length(md));
cg = zeros(length(wl),length(md));
u  = zeros(size(x));	v = u;      w = u;      eta = u;

%% Solve for the problem for different wavelengths and mode numbers
for cntr = 1:length(wl)
    k = 2*pi/wl(cntr);	k2 = k*k;	% Wavenumber of disturbance
    
    % Define A and B
    if any(U(:)) ~= 0
        LL = D2phys - k2*eye(NZ-1);
        A21 = sparse(diag(U)*LL - diag(Uzz));   A = [A11 A12; A21 A22];
        B21 = LL;                               B = [B11 B12; B21 B22];
    elseif f == 0
        B = D2phys - k2*eye(NZ-1);
    elseif f ~= 0
        A = (f/k)^2*D2phys - diag(N2);
        B = D2phys - k2*eye(NZ-1);
    end
    
    % Solve for the eigenvalue problem (assuming no instability)
    [phi,C] = eigs(A,B,max(md)*2,'lr'); % EIGS works for sparse matrices
    % Change 'lr' to 'largestreal' if using MATLAB R2017b or later.
    % Include the following options if EIGS fails to converge:
%     opts.p = 60;    opts.maxit = 1000;
%     [phi,C] = eigs(A,B,max(md)*2,'lr',opts);
    [cR,cRidx] = sort(real(diag(C)),'descend');
    if all(U(:)) == 0, cR = sqrt(cR); end
    cp(cntr,:) = cR(md);
    
    % Find the eigenvalue with the largest imaginary part (for instability)
%     opts.p = 60;    opts.maxit = 1000;
%     [phi,C] = eigs(A,B,max(md)*2,'li',opts);
%     [cI,cIidx] = sort(imag(diag(C)),'descend');
%     cIk(cntr) = cI(1)*k;
    
    % Find group velocity using central difference method
    if find_cg
        ktmp = k*(1+[-1e-6,1e-6]);	k2tmp = ktmp.*ktmp;
        for cntrtmp = 1:2
            if any(U(:)) ~= 0
                LL = D2phys - k2tmp(cntrtmp)*eye(NZ-1);
                A21 = sparse(diag(U)*LL-diag(Uzz));	A = [A11 A12; A21 A22];
                B21 = LL;                          	B = [B11 B12; B21 B22];                             B = [B11 B12; B21 B22];
            elseif f == 0
                B = D2phys - k2tmp(cntrtmp)*eye(NZ-1);
            elseif f ~= 0
                A = (f/ktmp(cntrtmp))^2*D2phys - diag(N2);
                B = D2phys - k2tmp(cntrtmp)*eye(NZ-1);
            end
            [~,Ctmp] = eigs(A,B,max(md)*2);
            [cRtmp,~] = sort(real(diag(Ctmp)),'descend');
            if all(U(:)) == 0, cRtmp = sqrt(cRtmp); end
            cptmp(cntrtmp,:) = cRtmp(md);
        end
        cg(cntr,:) = ...
            (cptmp(2,:)*ktmp(2)-cptmp(1,:)*ktmp(1))/(ktmp(2)-ktmp(1));
    end
    
    % Output parameters
    if cntr == 1
        fprintf(['%-12s','%-12s',...
            repmat(['%-',num2str(12*length(md)),'s'],1,2),'\n'],...
            'lambda (m)','k (1/m)','c_p (cm/s)','c_g (cm/s)');
        fprintf(['%-12s','%-12s',...
            repmat('mode-%g      ',1,length(md)),...
            repmat('mode-%g      ',1,length(md)),'\n'],...
            '','',md,md);
    end
    fprintf([repmat('%-12.2f',1,2+2*length(md)),'\n'],...
        wl(cntr),k,cp(cntr,:)*100,cg(cntr,:)*100);
    
    % Loop through different mode numbers to find the structure function
    for num_mode = md        
        phi_now  = phi(1:NZ-1,cRidx(num_mode));	% find the eigenfunction
        phi_now  = cR(num_mode)*phi_now/max(abs(phi_now));	% normalize
        phi_full = repmat([0;phi_now;0],1,NX);	% create full matrix in 2D
        phiz_now = Dphys*[0;phi_now;0];         % find the z-derivative
        phiz_full = repmat(phiz_now,1,NX);      % create full matrix in 2D
        
        % Apply no normal flow BCs to standing waves or generate random
        % numbers for superposition of traveling waves
        if standing
            phs = 0.5*pi;
        else
            phs = rand(1,1)*2*pi;
        end
        
        % Compute the u, w and rho fields
        % We don't need imaginary parts if we assume no instability
        u = u + cos(k*x+phs).*env(x,num_mode).*real(phiz_full); ...
%             - sin(k*xx+phs).*env(xx,num_mode).*imag(phiz_full);
        w = w + k*sin(k*x+phs).*env(x,num_mode).*real(phi_full) ...
            - cos(k*x+phs).*envx(x,num_mode).*real(phi_full); ...
%             + k*cos(k*xx+phs).*env(xx,num_mode).*imag(phi_full) ...
%             - sin(k*xx+phs).*envx(xx,num_mode).*imag(phi_full);
        eta = eta + cos(k*x+phs).*env(x,num_mode).*real(phi_full) ...
            ./ (cR(num_mode) - repmat(Uall,1,NX)); ...
%             - sin(k*xx+phs).*env(xx,num_mode).*imag(phi_full) ...
%             ./ (cR(num_mode) - repmat(md_u(zphys),1,NX));
        if all(U(:))==0 && f~=0
            v = v + f/(cR(num_mode)*k) ...
                * sin(k*x+phs).*env(x,num_mode).*real(phiz_full);
        end
    end
end
den = md_density(z-eta);

% Specify the direction of wave propagation and add background current
if ~right, u = -u; v = -v; w = -w; end
u = u + repmat(Uall,1,NX);

%% Plots

if test
    set(0,'defaulttextinterpreter','latex')
    
    figure(300), clf, colormap temperature
    set(gcf,'units','inches','position',[1 4.5 14 5.5])
    subplot(1,2,1)
    pcolor(x,z,den), shading flat
    axis([0 L 0 H])
    caxis(1+[-1 1]*(max(abs(den(:)))-1)), colorbar southoutside
    xlabel('$x$ (m)'), ylabel('$z$ (m)')
    title('(a) Density field')
    set(gca,'fontname','times','fontsize',18)
    
    subplot(1,2,2)
    pcolor(x,z,u), shading flat
    caxis([-1 1]*max(abs(u(:)))), colorbar southoutside
    hold on
    contour(x,z,den,[1,1],'--k','linewidth',1.2)
    axis([0 L 0 H])
    xlabel('$x$ (m)'), ylabel('$z$ (m)')
    title('(b) Horizontal velocity field')
    set(gca,'fontname','times','fontsize',18)
    
    if all(U(:))==0 && f~=0
        figure(301), clf, colormap temperature
        set(gcf,'units','inches','position',[1 4.5 7 5.5])
        pcolor(x,z,v), shading flat
        hold on
        contour(x,z,den,[1,1],'--k','linewidth',1.2)
        axis([0 L 0 H])
        caxis([-1 1]*max(abs(v(:)))), colorbar southoutside
        xlabel('$x$ (m)'), ylabel('$z$ (m)')
        title('v-velocity')
        set(gca,'fontname','times','fontsize',18)
    end
end
