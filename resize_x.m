function field = resize_x(Lx0, Nx0, Nz0, field_name, field_type, field, Nx1)
%% RESIZE_X  Increase or decrease the resolution of 'field' in the x dimension.
%
% Assumptions:
%  - if 2D, then must be in x-z plane
%  - Boundary condition must be free-slip or periodic
%    (periodic is not optimized since it will still extend the domain)
%
% Input parameters:
%  - field_name = 'u', 'w' or 'z'
%  - field_type = 'periodic' or 'free_slip'

% % read parameters
% params = spins_params();
% 
% % shorten some parameters
% type_x = params.type_x;
% Lx0 = params.Lx;
% Nx0 = params.Nx;
% Nz0 = params.Nz;
dx0 = Lx0/Nx0;
% try
%     Ny0 = params.Ny;
% catch
%     Ny0 = 1;
% end
Ny0 = 1;

% % error check
% if ~strcmp(type_x, 'FREE_SLIP') && ~strcmp(type_x, 'FOURIER')
%    error('X boundary condition is not FREE_SLIP or FOURIER.')
% end

% check sizes
mult = Nx1/Nx0;
if mult > 1
    increase_points = true;
else
    increase_points = false;
end

% permute to match SPINS output files
field = permute(field, [2 1]);
% reshape 2D to match the shape of a 3D field
if Ny0 == 1
    field = reshape(field, [Nx0 1 Nz0]);
end

% extension types
odd_ext  = @(f) [f; -flipud(f)];
even_ext = @(f) [f;  flipud(f)];
periodic_ext = @(f) [f; f];

% double the field
switch field_type
    case 'periodic'
    field = periodic_ext(field);
    otherwise
        if strcmp(field_name, 'u')
            field = odd_ext(field);
        else
            field = even_ext(field);
        end
end

% phase shift because grid is cell-centered, and take fft
dx1 = dx0/mult;
kxs = fftfreq(2*Nx0, dx0);
phase_corr = exp(-1i*kxs*(dx0/2-dx1/2)).';
field = bsxfun(@times, fft(field), phase_corr);

% pad or truncate
if increase_points
    % pad with zeros
    field = [field(1:Nx0,:,:); zeros(2*Nx1-2*Nx0,Ny0,Nz0); field(end-Nx0+1:end,:,:)];
else
    % truncate high frequencies
    field = [field(1:Nx1,:,:); field(end-Nx1+1:end,:,:)];
end

% take ifft
field = mult*real(ifft(field));
field = field(1:Nx1,:,:);
% return field to 2D if it was
if Ny0 == 1
    field = reshape(field,[Nx1 Nz0]);
end
field = permute(field, [2 1]);
