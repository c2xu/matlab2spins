%DRIVER     Input parameters and main driver file for MATLAB2SPINS2D.
% 
% Things have been done:
% 1) DJL solver,
% 2) TG solver for standing and traveling waves, with different horizontal
%    wavenumbers and vertical modes,
% 3) Poincare waves.
% 
% Things to add:
% 1) 3D, so a wave packet envelope could be A(x,y) and the waves can
%    propagate in any directions in the x-y plane,
% 2) Topography in z-direction,
% 3) Weakly nonlinear theory,
% 4) Instability in linear theory,
% 5) Rotation for solitary waves (3D and free-slip in y).
%
% For now, it only works for a 2D domain, no topography in z.
% The TG solver assumes no instability at this time. None-zero background
% current has not been tested yet.
%
% June 28, 2018: Before running cases of Poincare waves, make sure SPINS
%   can read in v-field. To do so, go to wave_reader.cpp, replace line 197
%   with the follows:
% 
%   if (Ny > 1 || rot_f != 0) {
%       init_field("v", v_filename, v, input_data_types);
%   }
%   
%   and rebuild wave_reader.x.
% 
% Last updated: June 29, 2018
%

clearvars

test = true;    % set to true if just testing, set to false and specify 
                % casename below to write data to disk

casename = '';  % specify the case name here

wave = 'djl';   % specify the type of waves here: 'lin', 'wnl' or 'djl'

%% Grid parameters

% MATLAB grid
L = 4;          % length of solitary/linear wave domain
H = 0.5;        % depth of solitary/linear wave domain, must be positive
NX = 512;       % x-grid of DJL/TG solver
NZ = 256;       % z-grid of DJL/TG solver (Cheb grid is used in TG solver)

% SPINS grid
Lx = L;         % length of computational domain
Lz = H;         % depth of computational domain
Nx = 4096;      % x-grid for SPINS
Nz = 256;       % z-grid for SPINS

%% Stratification

% Primary pycnocline
drho = 0.01;    % density difference
z0 = 0.4;      % pycnocline center
d = 0.01;      % pycnocline thickness

% Secondary pycnocline
drho2 = 0.0*drho;   % set to 0 if there is only one pycnocline
z02 = 0.19;
d2 = 0.0025;

% Density profile
md_density = @(z) 1 - 0.5*drho*tanh((z-z0)/d) - 0.5*drho2*tanh((z-z02)/d2);
md_d_density = @(z) - 0.5*drho *sech((z-z0 )/d ).*sech((z-z0 )/d )/d...
                    - 0.5*drho2*sech((z-z02)/d2).*sech((z-z02)/d2)/d2;
% n2 = @(z) 9.81*0.5*drho *sech((z-z0 )/d ).*sech((z-z0 )/d )/d...
%        + 9.81*0.5*drho2*sech((z-z02)/d2).*sech((z-z02)/d2)/d2;

%% Background current

% uamp=0.0;
md_u=@(z) zeros(size(z));
md_uz=@(z) zeros(size(z));
md_uzz=@(z) zeros(size(z));

%% Specifying input parameters and solving for the problem

switch wave
    case 'lin'
        standing = false;	% true (false) for standing (traveling) waves 
        % 'true': free-slip side walls, specify 'md_x' below
        % 'false': periodic side walls, specify 'wl' and below
        
        md_x = 2;       % horizontal mode number for standing waves
        wl   = 1;       % wavelength for traveling waves (can be a vector)
        % Note: Even for traveling waves, L must still be divisible by wl, 
        % unless appropriate envelop functions are specified.
        
        md  = 1;        % vertical mode number (can be a vector)
        amp = .01;      % maximum amplitude
        right = true;   % true (false) for rightward (leftward) propagation
        f   = 0e-3;     % rotation
        
        % Envelop function and its x-derivative
        env = @(x,num_mode) amp;
%             *(.5*tanh((x-L_left)/dd)/num_mode ...
%             - .5*tanh((x-L_right)/dd)/num_mode);
        envx = @(x,num_mode) 0*amp;
%             *(.5*(1 - tanh((x-L_left)/dd).^2)/dd/num_mode ...
%             - .5*(1 - tanh((x-L_right)/dd).^2)/dd/num_mode);
        
        find_cg = false; % set false to turn it off, since computing c_g 
                        % will slow things down significantly

        % Taylor-Goldstein solver, the z-grid is Chebyshev and is defined 
        % from H to 0 in decreasing order
        tg
        
    case 'wnl'
        return
        % to be added in the future...
        
    case 'djl'
        A = 1.5e-4;     % APE
        f = 0;          % no rotation for solitary waves
        if test            
            verbose = 1;    % show progress during solving
        else
            verbose = 0;
        end 
        
        % In the DJL solver, the z-grid is equally spaced and is defined
        % from H to 0 in decreasing order
        get_eta
        iswpost
        iswpic
end

% return  % comment out this line to proceed with interpolation

%% Interpolation

ydir = false;               % true for 3D, false for 2D (only 2D for now)
slip = true;                % true for free-slip, false for no-slip
method = 'spline';          % interpolation method
% 'no_interp': works for TG solver only if no-slip in z (i.e. slip==0),
%       works for DJL solver only if free-slip in z (i.e. slip==1).
% 'spectral': does not work for TG solver as it uses Cheb grid in z.
% 'nearest', 'linear', 'spline' and 'cubic': Matlab built-in interpolation
%       methods; 'spline' has the highest accuracy.

if ~ydir, spins_interp2d, end

%% SPINS grid ordering

g     = 9.81;
nu    = 1e-6;
kappa = 2e-7;
pert  = 0.00;
init_time  = 0;
final_time = 100;
plot_interval = 1;

if ~test, matlab2spins2d, quit, end
